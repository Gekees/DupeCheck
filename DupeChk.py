#! /usr/bin/python3

import os, sys, hashlib
from collections import defaultdict

# -h = help
# -d = given directory is target
# -r = removal, prompts with a list and for choosing which to delete
# -f = sends results to the given file
def options():
    writo=""
    fresul=False
    rem=False
    targetDir="."
    counter=1
    for i in sys.argv[0:]:
        if i=="-d":
            targetDir=sys.argv[counter]
        elif i=="-r":
            rem=True
        elif i=="-h":
            print("\nOptions:\n\t\"-d\"\tFollowed by the target directory.\n\t\"-r\"\tTo prompt for file removal\n\t\"-h\"\tHelp")
            print("\t\"-f\"\tFollowed by the file to send results to.\n\n")
            print("Usage:\n\tDupeChk.py -r -d <directory> -f <result file>\n")
            exit()
        elif i=="-f":
            fresul=True
            writo=sys.argv[counter]
        else:
            pass
        counter+=1
    return targetDir, rem, fresul, writo
# creates a hash value for each file in the given directory tree
# returns a tuple that of (hash, path)
def checksum(targ):
    md5=hashlib.md5()
    try:
        with open(targ, "rb") as t:
            for i in iter(lambda: t.read(1024), b""):
                md5.update(i)
    except FileNotFoundError as err:
        pass
    return md5.hexdigest(), targ
# loops through the given directory tree and creates a list of dictionaries:
# [hash : (path to file with that hash, path to file with that hash, etc.)
def hashing(tDir):
    ex, fs, ih, haLS, pLS=[], [], [], [], []
    fDict=defaultdict(list)
# rounds up files and their paths recursively throughout the given directory tree
# and makes a list of all paths to all files
    for start, direc, files in os.walk(tDir):
        for fls in files:
            fs.append(os.path.join(start, fls))
# loops the hashing of each file in the tree and adds those tuples to a list (hash, path)
    for i in fs:
        ih.append(checksum(i))
       # print("\n", ih, "\n")
# loops through all hash and path pairs, making a dictionary for each hash
# and appends all path with matching hashes into a list as the value for that hash
    for i in ih[0:]:
        fDict.setdefault(i[0], []).append(i[1])
# iterates through the dictionary of hashes as tuples and makes a list of entries which lack duplicates
# for removal, the exclusion list
    for i, ii in fDict.items():
        if len(ii)<2:
            ex.append(i)
# Uses the exclusion list to remove entries which lack duplicates
    for i in ex[0:]:
        fDict.pop(i)
    return fDict
# prompts user for a number corralating to the file they want to delete
def remo(fullList):
    forDel= []
#grabs the two elements of the tuples in the list of (hash, path[]) tuples
    for i, ii in fullList.items():
# uses space as the delimiter to split user input after displaying the what files are to be considered
        print ("\n",ii)
        forDel=input("\nBy number (starting at 1) which of these would you like to delete? ex:\"1 2 4\"\nor type \"all\" to remove all of these files.:\n\n").split()
# deletes all the given files going one-by-one through the list.
        if "all" in forDel:
            for x in ii:
                try:
                    os.remove(x)
                    print("\n",x, "Has been deleted.")
                except:
                    print("I can't remove that, maybe it's already gone.")
# checks that the input given is an integer
        else:
            try:
                rmList=[]
                for num in forDel:
                    rmList.append(int(num)-1)
            except:
                print("\nInvalid entry\n")
                pass
            for x in rmList:
                #!!!minor_testing_print(x, x+1, len(ii))
# checks that the given integer is in the index of the file list, removes the file, and displays confirmation
                rml=len(ii)
                if (x>=0) and (x+1<=rml):
                    try:
                        os.remove(ii[x])
                        print("\n",ii[x], "has been deleted.\n")
                    except:
                        print("I can't remove",ii[x], " maybe it's already gone.")
                else:
                    print("\n",x+1, "isn't in my index.\n")
                    pass
# function for printing results and sending them to a file
def verb(fuList, fresul, writo):
    strDict=""
    for i, ii in fuList.items():
        print("\n",i,"\n",ii)
    if fresul==True:
#        try:
        with open(writo, "w") as wresults:
            for i in fuList.items():
                strDict+=str(i)
                strDict+="\n\n\n"
            wresults.write(strDict)
#       except:
    else:
        pass
    print("\n\n",len(fuList),"unique, duplicated files found.\n")
    #!!!print(rem)
def main():
    tDir, rem, fresul, writo=options()
    fullList=hashing(tDir)
    if rem==True:
        remo(fullList)
    verb(fullList, fresul, writo)
if __name__=="__main__":
    try:
        main()
    except KeyboardInterrupt:
        print ("\n")
        exit()
